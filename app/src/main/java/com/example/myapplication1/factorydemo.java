package com.example.myapplication1;

import android.util.Log;

public class factorydemo {
    public void demo(){
        AbstractFactory miFactory = new XiaoMiFactory();
        AbstractFactory appleFactory = new AppleFactory();
        miFactory.makePhone();
        miFactory.makePC();
        appleFactory.makePhone();
        appleFactory.makePC();
    }
}

interface Phone {
    void make();
}
class MiPhone implements Phone {
    private static final String TAG = "factorydemo";
    public MiPhone() {
        this.make();
    }
    @Override
    public void make() {
        Log.d(TAG, "make: mip");
    }
}
class IPhone implements Phone {
    private static final String TAG = "factorydemo";
    public IPhone() {
        this.make();
    }
    @Override
    public void make() {
        Log.d(TAG, "make: ip");
    }
}
interface PC {
    void make();
}
class MiPC implements PC {
    private static final String TAG = "factorydemo";
    public MiPC() {
        this.make();
    }
    @Override
    public void make() {
        Log.d(TAG, "make: mipc");
    }
}
class MAC implements PC {
    private static final String TAG = "factorydemo";
    public MAC() {
        this.make();
    }
    @Override
    public void make() {
        Log.d(TAG, "make: mac");
    }
}
interface AbstractFactory {
    Phone makePhone();
    PC makePC();
}
class XiaoMiFactory implements AbstractFactory{
    @Override
    public Phone makePhone() {
        return new MiPhone();
    }
    @Override
    public PC makePC() {
        return new MiPC();
    }
}
class AppleFactory implements AbstractFactory {
    @Override
    public Phone makePhone() {
        return new IPhone();
    }
    @Override
    public PC makePC() {
        return new MAC();
    }
}
