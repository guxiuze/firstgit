package com.example.boltstask;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import bolts.Continuation;
import bolts.Task;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //当前
        Task.call(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Log.d(TAG, Thread.currentThread().getName());
                return true;
            }
        });
        //后台
        Task.callInBackground(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Log.d(TAG, Thread.currentThread().getName());
                return true;
            }
        });
        //ui主线程
        Task.call(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Log.d(TAG, Thread.currentThread().getName());
                return true;
            }
        }, Task.UI_THREAD_EXECUTOR);
        //延时
        Task.delay(2000).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(Task<Void> task) throws Exception {
                Log.d(TAG, Thread.currentThread().getName());
                return null;
            }
        });

        Task.call(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Log.d(TAG, Thread.currentThread().getName());
                return true;
            }
        }, Task.UI_THREAD_EXECUTOR).continueWith(new Continuation<Boolean, String>() {
            @Override
            public String then(Task<Boolean> task) throws Exception {
                Thread.sleep(2000);
                Log.d(TAG, Thread.currentThread().getName()+"on success");

                return "hello bolts";
            }
        }, Task.BACKGROUND_EXECUTOR).continueWith(new Continuation<String, Void>() {
            @Override
            public Void then(Task<String> task) throws Exception {
                Log.d(TAG, Thread.currentThread().getName()+"contunue with");
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);


        final Executor PARALLEL_EXECUTOR = Executors.newCachedThreadPool();

        Task.call(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                for (int i = 0; i < 100; i++) {
                    if (i % 2 == 0) {
                        Log.d(TAG, Thread.currentThread().getName()+"-"+i);
                    }
                }
                return null;
            }
        }, PARALLEL_EXECUTOR);

        Task.call(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                for (int i = 0; i < 100; i++) {
                    if (i % 2 == 1) {
                        Log.d(TAG, Thread.currentThread().getName()+"-"+i);
                    }
                }
                return null;
            }
        }, PARALLEL_EXECUTOR);




        //fifo
        final Executor SERIAL_EXECUTOR = Executors.newSingleThreadExecutor();

        Task.call(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                for (int i = 0; i < 50; i++) {
                    Log.d(TAG, Thread.currentThread().getName()+"-"+i);
                }
                return null;
            }
        }, SERIAL_EXECUTOR);

        Task.call(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                for (int i = 50; i < 100; i++) {
                    Log.d(TAG, Thread.currentThread().getName()+"-"+i);
                }
                return null;
            }
        }, SERIAL_EXECUTOR);
    }
}
