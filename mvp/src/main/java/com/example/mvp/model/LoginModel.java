package com.example.mvp.model;

import com.example.mvp.presenter.OnLoginFinishedListener;

public interface LoginModel {
    void login(User user, OnLoginFinishedListener listener);
}
